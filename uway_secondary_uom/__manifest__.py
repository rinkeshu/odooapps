# -*- coding: utf-8 -*-
{
    'name': 'Product Secondary UOM',
    'category': 'Base',
    'sequence': 1,
    'summary': "Product's second uom calculate.",
    'version': '14.0.0.0',
    'license': 'OPL-1',
    'depends': [
        'product','stock','uom'
    ],

    'data': [
        'security/security_rule.xml',
        'views/product_product_view.xml',
    ],

    'author': 'Utechweb',
    'maintainer': 'Rinkesh',

    'installable': True,
    'auto_install': False,
    'application': True,
    'price': 20.00,
    'currency': 'EUR',
}
