# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class product_product(models.Model):
    _inherit = 'product.product'

    is_secondaryuom = fields.Boolean('Secondary UOM Product')
    secondaryuom_id = fields.Many2one('uom.uom','Secondary UOM')
    onheand = fields.Float('On Heand Qty',compute="compute_qty")
    forcast = fields.Float('Forcast Qty',compute="compute_qty")
    uom_name = fields.Char(string='Secondary Unit of Measure Name', related='secondaryuom_id.name', readonly=True)

    def compute_qty(self):
        for record in self:
            if record.secondaryuom_id:
                record.onheand = record.secondaryuom_id._compute_price(record.qty_available,record.uom_id)
                record.forcast= record.secondaryuom_id._compute_price(record.virtual_available,record.uom_id)
            else:
                record.onheand = 0.00
                record.forcast= 0.00